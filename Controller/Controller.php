<?php
if (!defined("ROOT")) {
    define("ROOT", "../");
}
/**
 * Controller Class
 *
 * CRUD operation
 */

class Controller
{
    private $model;

    function __construct()
    {
        require ROOT . "Model/Model.php";

        $this->model = new Model();
    }

    function readAll()
    {
        return $this->model->readAll();
    }
    function readOne($id)
    {
        return $this->model->readOneById($id);
    }
    function newClick($date)
    {
        return $this->model->create($date);
    }
}

$ctrl = new Controller();
if (!empty($_GET)) {
    if (isset($_GET['q'])) {
        $type = htmlspecialchars($_GET['q']);
        if ($type == "readAll") {
            echo $ctrl->readAll();
        } elseif ($type == "readOne") {
            if (isset($_GET['id'])) {
                echo $ctrl->readOne(intval(htmlspecialchars($_GET['id'])));
            }
        } elseif ($type == "newClick") {
            date_default_timezone_set("Europe/Paris");
            echo $ctrl->newClick(date("Y-m-d H:i:s"));
        }
    }
}
