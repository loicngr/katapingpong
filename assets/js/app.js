/**
 * Simple Fetch request
 * @param {string} type
 * @return {{status: boolean, data: {}}}
 */
async function request(type) {
    let resp = await fetch(`./Controller/Controller.php?q=${type}`);
    let result = { status: false, data: false };
    if (resp.ok) {
        try {
            result = { status: true, data: await resp.json() };
        } catch (error) {
            result = { status: false, data: error };
        }
        return result;
    } else {
        result = { status: false, data: false };
        return result;
    }
}

/**
 * Make a Ping
 * @param {event} e
 */
function ping(e) {
    const orderSection = e.target.form.parentNode.id === "section-first" ? 1 : 0;

    request("newClick").then(response => {
        if (response.status) {
            pElements[orderSection].innerHTML = "pong";
        } else {
            pElements[orderSection].innerHTML = JSON.stringify(response.data);
        }
    });
}
/**
 * Make a Pong
 * @param {event} e
 */
function pong(e) {
    const orderSection = e.target.form.parentNode.id === "section-first" ? 1 : 0;

    request("newClick").then(response => {
        if (response.status) {
            pElements[orderSection].innerHTML = "ping";
        } else {
            pElements[orderSection].innerHTML = JSON.stringify(response.data);
        }
    });
}

/**
 * Print Total infos
 * @param {event} e
 */
function printInfos(e) {
    request("readAll").then(response => {
        alert(JSON.stringify(response.data));
    });
}

const pElements = document.querySelectorAll("form p");
const btns = document.querySelectorAll('form button[type="button"]');
document.querySelector("button#resultInfos").addEventListener("click", printInfos);
btns.forEach(btn => {
    if (btn.textContent === "PING") {
        btn.addEventListener("click", ping);
    } else if (btn.textContent === "PONG") {
        btn.addEventListener("click", pong);
    }
});
