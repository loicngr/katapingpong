<?php
if (!defined("ROOT")) {
    define("ROOT", "../");
}
/**
 * Model Class
 *
 * Connect to Database with PDO method
 */

class Model
{
    protected $mysql_config;
    protected $pdo;

    function __construct()
    {
        require_once ROOT . "env.php";

        $this->mysql_config = array(
            "login" => $MYSQL_LOGIN,
            "pass" => $MYSQL_PASS,
            "db" => $MYSQL_DBNAME,
            "host" => $MYSQL_HOST
        );

        $this->connection();
    }
    /**
     * Connection to my Database with PDO
     */
    private function connection()
    {
        try {
            $this->pdo = new PDO(
                "mysql:host={$this->mysql_config['host']};dbname={$this->mysql_config['db']}",
                $this->mysql_config["login"],
                $this->mysql_config["pass"]
            );
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            print "Erreur !: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    /**
     * Read All element in my Database
     * @return Object
     */
    public function readAll()
    {
        $rq = $this->pdo->prepare("SELECT * FROM compteur");
        $rq->execute();
        return json_encode($rq->fetchAll(PDO::FETCH_OBJ));
    }
    /**
     * Read juste one leement in my database with an id
     * @param [number] $id
     * @return Object
     */
    public function readOneById($id)
    {
        $rq = $this->pdo->prepare("SELECT * FROM compteur WHERE id = :id");
        $rq->bindParam(":id", $id);
        $rq->execute();
        return json_encode($rq->fetch(PDO::FETCH_OBJ));
    }
    /**
     * Insert element in my Database with Datetime
     * @param [datetime] $date
     * @return Object
     */
    public function create($date)
    {
        $result = [
            "status" => true
        ];
        try {
            $rq = $this->pdo->prepare("INSERT INTO compteur (created_at) VALUES (:date)");
            $rq->bindParam(":date", $date);
            $rq->execute();
        } catch (\Throwable $th) {
            $result = [
                "status" => false,
                "error" => $th
            ];
        }
        return json_encode($result);
    }
}
